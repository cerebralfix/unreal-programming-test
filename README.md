# CerebralFix Senior Unreal Engine Coding Test #

## Instructions ##

We would like you to create a version of a Breakout-style game using Unreal engine. We would like you to make this game in a 2.5D style - that is, using 3D assets for 2D gameplay. If you are unfamiliar with the game, there are many versions you can play for free online to get an idea of what we’re looking for. We do not expect you to spend a particularly long time on this - ideally no more than a few hours.

Finally, if any of these instructions aren’t clear or you have trouble getting the project set up, please feel free to reach out to us for help!

## Functional Requirements ##

1. The Ball
    - The ball should rebound off the sides, top of the game board and off the paddle.
2. The Paddle
    - Moves left and right with input.
3. The Player
    - If the ball falls of the bottom of the game board, a life is lost and the ball is reset.
    - After all lives are lost the game ends.
4. The Game
    - Should have atleast the follow:
        - Has a main menu
        - Atleast 1 level
        - Restarts correctly during subsequent playthroughs without needing to close and reopen the game.
        - The game should be playable in the editor & as standalone game.
5. Bricks
    - Objects that the ball can hit and bounce off, the level should demostrate each type of brick:
        - Standard Brick:
            - Once hit the brick is destroyed, and the ball is rebounded.
        - Multi Hit Brick:
            - The brick needs to be hit multiple times before being destroyed.
        - The Moving Brick:
            - A brick that moves left and right across the level. 1 Hit to destroy.
        - The Brick Brick:
            - An indestructible brick that cannot be destroyed.
6. Paddle Boost
    - If the player clicks when the ball is near the paddle, the ball is accelerated away from the paddle at a higher velocity.

## What We’re Looking For ##

We are looking at your C++ ability as much as your knowledge of Unreal Engine, so having an idea where you draw the line between C++ and Blueprints given the following:

- The process you used to complete this test.
- Good use of Object Orientated design.
- Code reusability.
- Extensibility and maintainability of the software.
- Use of appropriate Unreal Engine capabilities.
- Appropriate use of C++ and Blueprints.
- Use of best practises.

## Deliverables ##

- The source for your project hosted on [bitbucket.org](https://bitbucket.org).
- Instructions on how to play your game (including which version of Unreal you used to create it)
- You are welcome to include a packaged version of your game, although this is not a necessity. Either way, we do require access to the source code

## Technology ##

- You should use a recent version of Unreal - 4.27.2 or higher
- You are welcome to use third-party assets, as long as they do not include game logic code. If you do include any such assets, please provide a list of where you obtained these assets with your submission.

## Next Steps ##

1. Install latest Unreal Engine 4, (4.27.2 or Higher)
1. [Fork](../../fork) this repository to your bitbucket account as a private repository, then clone it to your local machine.
1. Implement the required functionality.
1. Submit to your repository as needed.
1. Once you are satisfied with your submission, ensure keegan@cerebralfix.com & kris@cerebralfix.com has access to the repository, and then reply to the email from HR to let us know! We’ll get in touch once we’ve had a chance to review it.

## Extra Thoughts ##

Once you have finished your submission, think about this question:

- How would you implement multiple levels in this project?
